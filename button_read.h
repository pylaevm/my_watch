#ifndef _BUTTON_READ_H_
#define _BUTTON_READ_H_

#include <Arduino.h>

void button_tune_init ();

uint8_t button_tune_hit_count ();

void button_tune_hit_reset ();

void button_tune_hit_set (uint8_t hit_count);

void button_mode_init ();

void button_mode_read (int &state);

#endif //_BUTTON_READ_H_

