#include "light_read.h"
BH1750 lightMeter;

void 
light_init ()
{
  lightMeter.begin();
}

uint16_t 
light_read ()
{
  return lightMeter.readLightLevel();
}
