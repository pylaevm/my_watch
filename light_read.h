#ifndef _LIGHT_READ_H_
#define _LIGHT_READ_H_
#include <BH1750.h>

void light_init ();
uint16_t light_read ();

#endif //_LIGHT_READ_H_

