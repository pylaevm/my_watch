#ifndef _DISPLAY_H_
#define _DISPLAY_H_

/* Отображение информации на дисплее 0.96" */
#include <Arduino.h>

/* Инициализация дисплея - отображение логотипа */
bool display_init ();

/* Отображение времени 15:59 */
void display_time (uint8_t hour, uint8_t minute);

/* Отображение даты 19/02/15 */
void display_date (uint8_t day, uint8_t month, uint16_t year);

/* Отображение настройки */
void display_tune (int param);

/* Отображение ошибки */
void display_error (char *what);

void display_temperature_humidity_f (float temperature, float humidity);

void display_temperature_humidity (int temperature, int humidity);

/* level can be 1-3 */
void display_tune_bright (uint8_t level);

/* Очистка экрана */
void display_clear ();

/* Тестирование дисплея для отображения времени */
void test_display ();


#endif //_DISPLAY_H_

