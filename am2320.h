#ifndef _AM2320_H_
#define _AM2320_H_
#include <AM2320.h>

bool am2320_init ();

void am2320_read (float *temperature, float *humidity);

#endif //_AM2320_H_
