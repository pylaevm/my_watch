#include "sonic_read.h"


unsigned long sonic_length ();

void
sonic_init (unsigned long *limit_length)
{
    pinMode(8, OUTPUT); 
    pinMode(9, INPUT);

    unsigned long cm = 0;
    unsigned long cm_sum = 0;
    unsigned long cm_sk = 0;
    unsigned long sko = 0;
    long diff;
  do
  {
    cm = 0;
    cm_sum = 0;
    cm_sk = 0;
    sko = 0;

    for (int i = 0; i < 100; i++)
    {
      cm = sonic_length();
      cm_sum += cm;
      cm_sk += cm*cm;
      //delay (5);
    }

    cm_sum = cm_sum/100;
    cm_sk  = cm_sk/100;
    sko = sqrt(cm_sk - pow(cm_sum,2));
    
    Serial.print("sko "); 
    Serial.print(sko);
    Serial.print(" avr ");
    Serial.println(cm_sum);  
    diff = (signed)sko - (signed)cm_sum;
  } while (diff > 0);

  *limit_length = cm_sum;
}

unsigned long
sonic_length ()
{
  unsigned long duration, cm; 
  digitalWrite(8, LOW); 
  delayMicroseconds(2); 
  digitalWrite(8, HIGH); 
  delayMicroseconds(10); 
  digitalWrite(8, LOW); 
  duration = pulseIn(9, HIGH); 
  cm = duration / 58;
  //if (cm == 0)
    {
      Serial.print(cm); 
      Serial.println(" cm"); 
    }
  return cm;
}

