#include "tune.h"
#include "button_read.h"
#include "display.h"
#include "DS3231.h"
#include "clock.h"


byte tune_cnt = 0;             /* Количество нажатий на 1ю кнопку, которая переключает режимы настройки */
int tune_set_val = -1;		   /* Параметр для записи в датчик */
int last_button_state = LOW;   /* Состояние кнопки режимов */

bool set_tune   (const ModeTune &mode);

void prepare_param (const ModeTune &tune_mode);

void
tune_init ()
{
  button_tune_init ();
  button_mode_init ();
}

void
check_tune (ModeView &mode)
{
  int state;
  button_mode_read (state);
  
  /* Если состояние кнопки изменилось */
  if (state != last_button_state)
  {
  	/* Включаем режим настройки параметров */
    mode = MODE_TUNE;

    /* По сути счетчик нажания на кнопку */
  	tune_cnt++;

    /* Установка текущего значения параметра в счетчик 2й кнопки, чтобы начать настройку с него, а не с нуля */
    prepare_param ((ModeTune)tune_cnt);

    /* При переключении режимов настройки 
       запишем предыдущий настроенный параметр в соотв. датчик */
    set_tune ((ModeTune)tune_cnt);

    last_button_state = state;

  	/* Прощелкали кнопкой по всем параметрам - выйдем из режима настройки */
    if (tune_cnt  > MODE_TUNE_BRIGHT)
      {
        tune_cnt = 0;
        mode = MODE_NONE;
        tune_set_val = -1;
      }
  }

}

ModeTune
read_tune (int &param)
{
  int hit = button_tune_hit_count ();
  bool reset_hit = false;

  /* Выставим признак ограничения для конкретного параметра, если превысили порог */
	switch (tune_cnt)
		{
		case MODE_TUNE_HOUR:
			if ( hit > 23)
			  reset_hit = true;
			break;

		case MODE_TUNE_MINUTE:
			{
			if (hit > 59)
			  reset_hit = true;
			break;
			}

		case MODE_TUNE_DAY:
			{
			if (hit > 31)
	  		reset_hit = true;
			break;
			}

		case MODE_TUNE_MONTH:
			{
			if (hit > 12)
			  reset_hit = true;
			break;
			}

		case MODE_TUNE_YEAR:
			{
			if (hit > 99)
			  reset_hit = true;
			break;
			}

		case MODE_TUNE_BRIGHT:
			{
			if (hit > 3)
			  reset_hit = true;
			break;
			}

		default:
			break;
		}

  /* Сбросим значение счетчика второй кнопки, 
     если вышли за пределы настройки по определенному параметру настройки */
	if (reset_hit)
	  button_tune_hit_reset ();

  /* Прочитаем заново значение второй кнопки */
	tune_set_val = button_tune_hit_count ();
	param = tune_set_val;

	return (ModeTune)tune_cnt;
}


void
prepare_param (const ModeTune &tune_mode)
{
  /* Исключаем преднастройку яркости дисплея для простоты. Только часы. */
  button_tune_hit_set (get_param (tune_mode));
}

bool
set_tune (const ModeTune &mode)
{
  /* Если что-то уже настраивали */
  if (tune_set_val != -1)
    {
      
      switch (mode)
        {

      	/* Если сейчас режим настройки минут, 
      	то до этого настраивали часы - их и запомним в DS3231 */
        case MODE_TUNE_MINUTE:
          set_param (MODE_TUNE_HOUR, tune_set_val);
          break;

        case MODE_TUNE_DAY:
          set_param (MODE_TUNE_MINUTE, tune_set_val);
          break;

        case MODE_TUNE_MONTH:
          set_param (MODE_TUNE_DAY, tune_set_val);
          break;

        case MODE_TUNE_YEAR:
          set_param (MODE_TUNE_MONTH, tune_set_val);
          break;

        case MODE_TUNE_BRIGHT:
          set_param (MODE_TUNE_YEAR, tune_set_val);
          break;

        case MODE_TUNE_BRIGHT + 1:
          display_tune_bright (tune_set_val); 
          break;

        }
    }
}

