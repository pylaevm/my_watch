#include <Wire.h>
#include "am2320.h"

AM2320 th;
long last_time = 0;

bool 
am2320_init ()
{
     
  if (0 != th.Read ()) 
  {
    delay(1000);
    if (0 != th.Read ())
        return false;
  }
  return true;
}

void
am2320_read (float *temperature, float *humidity)
{
  if (millis () - last_time > 2000)
  {
    th.Read ();
    last_time = millis ();
    *temperature = th.t;
    *humidity = th.h;
  }
}
