#include "button_read.h"
/* 
This example code is in the public domain.
http://www.arduino.cc/en/Tutorial/Debounce
*/

// Variables will change:
int tune_button_state;             // the current reading from the input pin
uint8_t tune_hit_count = 0;             // the current reading from the input pin
int last_tune_button_state = LOW;   // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long last_tune_debounce_time = 0;  // the last time the output pin was toggled

int mode_state = LOW;               // the current state of the output pin
int mode_button_state;              // the current reading from the input pin
int last_mode_button_state = LOW;   // the previous reading from the input pin
long last_mode_debounce_time = 0;   // the last time the output pin was toggled

void
button_tune_init ()
{
  pinMode(3, INPUT);
}

void
button_tune_hit_reset ()
{
  tune_hit_count = 0;
}

void
button_tune_hit_set (uint8_t hit_count)
{
  tune_hit_count = (hit_count > 255) ? 0 : hit_count; 
}

uint8_t
button_tune_hit_count ()
{
// read the state of the switch into a local variable:
  int reading = digitalRead(3);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != last_tune_button_state)
  {
    // reset the debouncing timer
    last_tune_debounce_time = millis();
  }

  if ((millis() - last_tune_debounce_time) > 50) 
  {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != tune_button_state) 
    {
      tune_button_state = reading;

      // only toggle the LED if the new button state is HIGH
      if (tune_button_state == HIGH) 
      {
        tune_hit_count++;
      }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  last_tune_button_state = reading;
  return tune_hit_count;
}


void 
button_mode_init ()
{
  pinMode(2, INPUT);
}

void 
button_mode_read (int &state)
{
  // read the state of the switch into a local variable:
  int reading = digitalRead(2);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH),  and you've waited
  // long enough since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != last_mode_button_state)
  {
    // reset the debouncing timer
    last_mode_debounce_time = millis();
  }

  if ((millis() - last_mode_debounce_time) > 50) 
  {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != mode_button_state) 
    {
      mode_button_state = reading;

      // only toggle the LED if the new button state is HIGH
      if (mode_button_state == HIGH) 
      {
        mode_state = !mode_state;
      }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  last_mode_button_state = reading;
  state = mode_state;
}


