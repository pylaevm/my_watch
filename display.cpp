#include "display.h"
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#include <Fonts/FreeSans12pt7b.h>
#include "light_read.h"

Adafruit_SSD1306 display(4);

#if (SSD1306_LCDHEIGHT != 64)
#error ("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

/* Отображение чисел в формате 05 06 итд. с дополняющим нулем */
String printDigits (int digits);

/* Настройка яркости дисплея по датчику освещенности */
void 
display_tune_bright_by_ligth ()
{
  uint8_t val = map (light_read (), 0, 5000, 0, 255);
  
  display.ssd1306_command (SSD1306_SETVCOMDETECT);
  display.ssd1306_command (0); 
  
  /* Out this to tune according light sensor */
  display.ssd1306_command (SSD1306_SETCONTRAST);
  display.ssd1306_command (val); 
}

bool 
display_init ()
{
  light_init ();
  
  display.setFont (&FreeSans12pt7b);
  // initialize with the I2C addr 0x3D (for the 128x64)
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.ssd1306_command(SSD1306_SETPRECHARGE);
  display.ssd1306_command(0x10); 
  
  display.ssd1306_command(SSD1306_SETVCOMDETECT);
  display.ssd1306_command(0); 
  
  /* Out this to tune according light sensor */
  display.ssd1306_command(SSD1306_SETCONTRAST);
  display.ssd1306_command(0x1); 
  
  // init done
  display.display();
  delay(3000);

  display.clearDisplay();
  display.setTextColor(WHITE);

  //turn on if need to check display time 
  //test_display (&display);
}

void 
display_time (uint8_t hour, 
              uint8_t minute)
{
  display_tune_bright_by_ligth ();
  display.clearDisplay ();
  display.setTextSize (2);
  display.setCursor (6, 47);
  display.print (printDigits (hour));
  display.print (":");
  display.print (printDigits (minute));
  display.display ();
}

void 
display_date (uint8_t day,
              uint8_t month,
              uint16_t year)
{
  display_tune_bright_by_ligth ();
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20,40);
   
  display.print(printDigits (day));
  display.print("/");
  display.print(printDigits (month));
  display.print("/");
  display.print(printDigits (year));
  display.display();
}

void
display_tune (int param)
{
  display.clearDisplay ();
  display.setTextSize (1);
  display.setCursor (20,40);   
  display.print ("tune ");
  display.print (param);
  display.display ();
}

void
display_error (char *what)
{
  display.clearDisplay ();
  display.setTextSize (1);
  display.setCursor (20,40);   
  display.print ("error ");
  display.print (what);
  display.display ();
}

void
display_temperature_humidity_f (float temperature,
                                float humidity)
{
  display_tune_bright_by_ligth ();
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20,20);
   
  display.print("t ");
  display.print(temperature);
  display.print(" C");
  
  display.setCursor(20,50);
   
  display.print("h ");
  display.print(humidity);
  display.print(" %");
  display.display();
}

void 
display_temperature_humidity (int temperature,
                              int humidity)
{
  display_tune_bright_by_ligth ();
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20,20);
   
  display.print("t ");
  display.print(temperature);
  display.print("* C");

  display.setCursor(20,50);
   
  display.print("h ");
  display.print(humidity);
  display.print(" %");
  display.display();
}

void
display_tune_bright (uint8_t level)
{
  switch (level)
    {
    case 1:
      display.ssd1306_command(SSD1306_SETPRECHARGE);
      display.ssd1306_command(0x10); 
      break;

    case 2:
      display.ssd1306_command(SSD1306_SETPRECHARGE);
      display.ssd1306_command(0x19); 
      break;

    case 3:
      display.ssd1306_command(SSD1306_SETPRECHARGE);
      display.ssd1306_command(0x22); 
      break;
    }
}

void
display_clear ()
{
  display.clearDisplay();
  display.display();
}

void 
test_display ()
{     
  int h = 0, m = 0;
  
  for (;h < 24; h++)
    {
      m = 0;
      for (;m < 60; m++)
        {
            display.clearDisplay();
            display.setCursor(6,47);
            display.print(printDigits (h));
            display.print(":");
            display.print(printDigits (m));
            display.display();
            delay (5);
        }
    }  
}


String printDigits(int digits)
{
  String out;
  String zero = String('0');
  if(digits < 10)
    out = String (zero + digits);
  else
    out = String (digits);
  
  return out;
}

