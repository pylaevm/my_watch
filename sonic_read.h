#ifndef _SONIC_READ_H_
#define _SONIC_READ_H_

#include <Arduino.h>

void sonic_init (unsigned long *limit_length);
unsigned long sonic_length ();

#endif //_SONIC_READ_H_

