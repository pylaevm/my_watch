#include "clock.h"
#include "DS3231.h"
DS3231 clock;

bool 
set_param (ModeTune mode, byte param)
{

  switch (mode)
    {
    case MODE_TUNE_HOUR:
      clock.setHour (param);
      break;  

    case MODE_TUNE_MINUTE:
      clock.setMinute (param);
      break;

    case MODE_TUNE_DAY:
      clock.setDate (param);
      break;

    case MODE_TUNE_MONTH:
      clock.setMonth (param);
      break;

    case MODE_TUNE_YEAR:
      clock.setYear (param);
      break;

    default:
      return false;
      break;
    }
  return true;
}

byte
get_param (ModeTune mode)
{
  byte ret = 255;

  switch (mode)
    {
    case MODE_TUNE_HOUR:
      {
      bool h12,am;
      ret = clock.getHour (h12, am);
      break;
      }

    case MODE_TUNE_MINUTE:
      ret = clock.getMinute ();
      break;


    case MODE_TUNE_DAY:
      ret = clock.getDate ();
      break;


    case MODE_TUNE_MONTH:
      {
      bool century;
      ret = clock.getMonth (century);
      break;
      }

    case MODE_TUNE_YEAR:
      ret = clock.getYear ();
      break;    }

  return ret;
}

