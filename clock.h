#ifndef _CLOCK_H_
#define _CLOCK_H_

#include "modes.h"
#include <Arduino.h>

bool set_param (ModeTune mode, byte param);

byte get_param (ModeTune mode);

#endif // _CLOCK_H_
 

