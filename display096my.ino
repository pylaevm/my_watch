#include <avr/sleep.h>

#include "display.h"
#include "tune.h"
#include "sonic_read.h"
#include "am2320.h"
#include "clock.h"

ModeView mode;
unsigned long limit_length = 0;

void  check_sonic ();
void  view_none ();   
void  view_time ();
void  view_date ();
void  view_temperature_humidity ();
void  view_tune ();
void  view_error ();
  
void 
setup () 
{
  Serial.begin (9600);
  
  sonic_init (&limit_length); 
  display_init ();
  am2320_init ();
  tune_init ();
  // setSyncProvider( requestSync);  //set function to call when sync required
  // put your setup code here, to run once:
  Serial.print ("Limit length is ");
  Serial.println (limit_length);
  mode = MODE_NONE;
  set_sleep_mode (SLEEP_MODE_IDLE);
}

void 
loop () 
{
  check_sonic ();

  /* Проверка режима настройки последняя - чтобы всегда работать в этом режиме, если он есть */
  check_tune (mode);

  switch (mode)
    {
    case MODE_NONE:
      view_none ();
      break;
    case MODE_TIME:
      view_time ();
      break;
    case MODE_DATE:
      view_date ();
      break;
    case MODE_TEMPERATURE:
      view_temperature_humidity ();
      break;
    case MODE_TUNE:
      view_tune ();
      break;
    case MODE_ERROR:
      view_error ();
      break;
    };
  //delay (20);
}

void 
check_sonic ()
{
  unsigned long cm;
  static unsigned long last_time_to_view_any = 0;
  
  if (mode == MODE_TUNE)
    return;
    
  if ((millis () - last_time_to_view_any) < 3000)
    return;

  cm = sonic_length ();
  
  if (cm >= limit_length)
    {
      mode = MODE_NONE;
    }
  else if (cm >= (limit_length/2) && cm < limit_length - limit_length/7) 
    {
      mode = MODE_TIME;
      last_time_to_view_any = millis ();
    }
  else if (cm < (limit_length/2) && cm >= (limit_length/4))
    {
      mode = MODE_DATE;
      last_time_to_view_any = millis ();
    }
  else if (cm < (limit_length/4) && cm > 0)
    {
      mode = MODE_TEMPERATURE;
      last_time_to_view_any = millis ();
    }
  else if (cm == 0)
    {
      mode = MODE_ERROR;
      last_time_to_view_any = millis ();
    }
}

void 
view_none ()
{
  display_clear ();
}

void 
view_time ()
{ 
  display_time (get_param (MODE_TUNE_HOUR), get_param (MODE_TUNE_MINUTE));
}

void 
view_date ()
{
  display_date (get_param (MODE_TUNE_DAY), get_param (MODE_TUNE_MONTH), get_param (MODE_TUNE_YEAR));
}

void
view_temperature_humidity ()
{
  static float temperature = 0, humidity = 0;
  am2320_read (&temperature, &humidity);
  display_temperature_humidity_f (temperature, humidity);
}


void 
view_tune ()
{
  int cm = 0;
  ModeTune param = read_tune (cm);
  
  switch (param)
    {
    case MODE_TUNE_HOUR:
      display_time (cm, get_param (MODE_TUNE_MINUTE)); 
      break;
      
    case MODE_TUNE_MINUTE:
      {
        bool h12,am;
        display_time (get_param (MODE_TUNE_HOUR), cm); 
        break;
      }
      
    case MODE_TUNE_DAY:
      {
        bool century;     
        display_date (cm, get_param (MODE_TUNE_MONTH), get_param (MODE_TUNE_YEAR)); 
        break;
      }
    
    case MODE_TUNE_MONTH:
      {
        display_date (get_param (MODE_TUNE_DAY), cm, get_param (MODE_TUNE_YEAR)); 
        break;
      }

    case MODE_TUNE_YEAR:
      {
        bool century;
        display_date (get_param (MODE_TUNE_DAY), get_param (MODE_TUNE_MONTH), cm); 
        break;
      }

    case MODE_TUNE_BRIGHT:
      {
        display_tune (cm); 
        break;
      }
    
    default:
      display_tune (-1);
      break;
    };  
}

void 
view_error ()
{
  display_error ("US");
}
